"on: [1.8.8]"
"""""""""""""""""""""""""""""""""
"Authors: [Karlkorp, Inc. 2014; lhark, Inc. 2015]"
""""""""""""""""""""""""""""""""""""""""""""""""
"Type: [Vim/GVim/MacVim configuration file]"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Name: [.vimrc (unix/mac); _vimrc (MS Windows/DOS)]"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Location: [$HOME/.vimrc (unix/mac); $USERNAME\_vimrc (MS Windows)]"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"[Plug-ins list]""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"+------------------+-------------------------------------------------+"
"| AutoComplPop     | https://github.com/vim-scripts/AutoComplPop     |"
"| DelimitMate      | https://github.com/Raimondi/delimitMate         |"
"| DetectIndent     | https://github.com/roryokane/detectindent       |"
"| Multiple-Cursors | https://github.com/terryma/vim-multiple-cursors |"
"| Python-Mode      | https://github.com/klen/python-mode             |"
"| Surround         | https://github.com/tpope/vim-surround           |"
"| Undotree         | https://github.com/mbbill/undotree              |"
"| Vim-Airline      | https://github.com/bling/vim-airline            |"
"| Vim-GLSL         | https://github.com/tikhomirov/vim-glsl          |"
"| Vim-Gnupg        | https://github.com/jamessan/vim-gnupg           |"
"| Vundle           | https://github.com/gmarik/Vundle.vim.git        |"
"+------------------+-------------------------------------------------+"
"[Colorschemes list]""""""""""""""""""""""""""""""""""""""""""""""""""""
"+------------------+-------------------------------------------------+"
"| Solarized | https://github.com/altercation/vim-colors-solarized |"
"+------------------+-------------------------------------------------+"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"[To disable compatibility with Vi]"
set nocompatible
"[Use Plug-ins and Plug-in Manager(Vundle) only on UNIX or MAC OS]"
if has("unix") || has("mac")
    filetype off
    "[Download and install Vundle Plug-in Manager]"
    try
        if !isdirectory(expand("$HOME/.vim/bundle"))
            silent !mkdir -p $HOME/.vim/bundle
            silent cd $HOME/.vim/bundle
            silent !git clone https://github.com/gmarik/Vundle.vim.git
            silent cd $HOME
        endif
        "[Run Plug-ins]"
        set runtimepath+=$HOME/.vim/bundle/Vundle.vim
        call vundle#begin()
        Plugin 'gmarik/Vundle.vim'
        map <F2> :VundleInstall<CR>
        map <F3> :VundleUpdate<CR>
        if has("python")
            Plugin 'klen/python-mode'
            let g:pymode_rope_lookup_project = 0
            let g:pymode_rope = 0
        endif
        if has("ruby")
            Plugin 'terryma/vim-multiple-cursors'
        endif
        Plugin 'mbbill/undotree'
        map <F5> :UndotreeToggle<CR>
        Plugin 'vim-scripts/LaTeX-Box'
        map <LocalLeader>ll :Latexmk<CR>
        if isdirectory(expand("/home/lhark/.vim/bundle/YouCompleteMe"))
            "[Workaround for YCM non-portability]"
            Plugin 'Valloric/YouCompleteMe'
        else
            Plugin 'AutoComplPop'
        endif
        Plugin 'vim-airline/vim-airline-themes'
        Plugin 'vim-airline/vim-airline'
        Plugin 'godlygeek/tabular'
        Plugin 'tikhomirov/vim-glsl'
        Plugin 'jamessan/vim-gnupg'
        Plugin 'davidhalter/jedi-vim'
        Plugin 'petRUShka/vim-opencl'
        Plugin 'Raimondi/delimitMate'
        Plugin 'tpope/vim-surround'
        Plugin 'captbaritone/better-indent-support-for-php-with-html'
        "[Autodetect indent style of a file]"
        let g:detectindent_preferred_indent=2
        let g:detectindent_autodetect=1
        Plugin 'roryokane/detectindent'
        "[Solarized theme]"
        Plugin 'altercation/vim-colors-solarized'
        call vundle#end()
    catch
    endtry
else
    filetype on
endif
"[Recognize the type/syntax of the file]"
filetype plugin on
filetype indent on
set omnifunc=syntaxcomplete#Complete
"[Enable built-in plug-ins/macros]"
runtime macros/matchit.vim
"[Enable syntax highlighting]"
syntax on
"[Read the changes after the save .vimrc]"
if has("autocmd")
    autocmd! BufWritePost $MYVIMRC source $MYVIMRC
endif
"[Define the leader key]"
let mapleader=","
let maplocalleader=","
"[List of buffers]"
map <F7> :ls!<CR>:buffer<Space>
"[Reselect visual block after indent/outdent]"
vnoremap < <gv
vnoremap > >gv
"[Improve up/down movement on wrapped lines]"
nnoremap j gj
nnoremap k gk
" nnoremap <Down> gj
" nnoremap <Up> gk
" vnoremap <Down> gj
" vnoremap <Up> gk
" inoremap <Down> <C-o>gj
" inoremap <Up> <C-o>gk
"[Clear search highlights]"
nnoremap // :nohlsearch<CR>
"[Easy split navigation]"
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-h> <C-W>h
nnoremap <C-l> <C-W>l
nnoremap <C-Up> <C-W>k
nnoremap <C-Down> <C-W>j
nnoremap <C-Left> <C-W>h
nnoremap <C-Right> <C-W>l
"[Locate the desired objects in the center of the screen]"
nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <silent> * *zz
nnoremap <silent> # #zz
"[New line under/bellow current line without jump to insert-mode]"
nnoremap <leader>o o<Esc>
nnoremap <leader>O O<Esc>
"[Auto-complete parenthesis, brackets and braces, quotes]"
"inoremap " ""<Left>
"inoremap ' ''<Left>
"inoremap ( ()<Left>
"inoremap [ []<Left>
"inoremap { {}<Left>
"[Easy jump into the normal-mode from the insert-mode]"
inoremap jj <Esc>
"[To move over parenthesis, brackets, quotes in insert-mode]"
inoremap kk <Right>
inoremap hh <Left>
"[Easy omni-completion with Ctrl-Space]"
"inoremap <C-Space> <C-X><C-O>
"[To disable the arrow keys]"
for prefix in ['i', 'n', 'v']
    for key in ['<Up>', '<Down>', '<Left>', '<Right>']
        execute prefix . "noremap " . key . " <Nop>"
    endfor
endfor
"[Threshold for reporting number of lines changed]"
set report=0
"[Show command in the last line of the screen]"
set showcmd
"[Show both the tag name and a tidied-up form of the search pattern]"
set showfulltag
"[Don't give the intro message when starting Vim]"
set shortmess=""
if has("unix") || has("mac")
    set shortmess+=I
endif
"[Always show StatusLine]"
set laststatus=2
"[Define StatusLine]"
set statusline=""
set statusline+=%F%m%r%h%w
set wrapscan
set statusline+=%=%y
set statusline+=\ [%{&ff}]
set statusline+=\ Line:%l/%L
set statusline+=\ Column:[%v]
set statusline+=\ Buffer:[%n]
"set statusline+=\ Mode:[%{ShowModeInStatusLine()}]
"[Splitting rules]"
set splitbelow
set splitright
set equalalways
"[Lisp coding settings]"
if (&filetype == "lisp")
    set lisp
endif
"[Use the mouse in terminal]"
set mouse=a
set mousemodel=extend
"[Hide mouse while printing the text]"
set mousehide
"[Visualisation settings]"
set background=dark
set ttyfast
set showmode
set tabline=""
" set cmdheight=1 : default
set showtabline=0
set colorcolumn=""
set nocursorcolumn
set cmdwinheight=10
set virtualedit=all
"[GUI/Color Scheme/Font settings]"
if has("gui_running")
    winsize 90 50
    silent cd $HOME
    set linespace=0
    set guioptions=""
    set guitablabel=""
    if has("autocmd")
        autocmd InsertEnter * set cursorline
        autocmd InsertLeave * set nocursorline
    endif
    if has("win32") || has("win64")
        try
            colorscheme solarized
            set guifont=PT_Mono:h12
        catch
        endtry
    elseif has("unix")
        try
            colorscheme solarized
            set guifont=Liberation\ Mono\ 12
        catch
        endtry
    elseif has("mac")
        try
            set antialias
            colorscheme solarized
            set guifont=Monaco:h12
        catch
        endtry
    endif
else
    if has("unix") || has("mac")
        try
            set t_Co=256
            let g:solarized_termcolors=256
            colorscheme solarized
        catch
        endtry
    endif
endif
"[Backspace functions]"
set backspace=indent,eol,start
"[Scroll settings]"
set scrolloff=10
set scrolljump=10
set showmatch
set matchpairs=":"
set matchpairs+=(:)
set matchpairs+={:}
set matchpairs+=[:]
"set matchpairs+=<:> : too annoying
"[Turn backup off and value of history]"
set nobackup
set noswapfile
set history=1000
set nowritebackup
set undolevels=5000
"[Indent & Tab/mode-line settings]"
set nopaste
set modeline
set smarttab
set tabstop=2
set expandtab
set autoindent
set copyindent
set smartindent
set cinwords=except,finally,def,class,with,do,if,elif,else,for,while,try,switch
set cino=g0,h2,i2
set modelines=1
set shiftwidth=2
set softtabstop=2
"[TextWidth settings]"
set textwidth=0
set wrapmargin=0
"[Settings of line numbers]"
set number
set numberwidth=2
"[Don't show current position]"
set ruler
"[For regular expressions turn magic on]"
set magic
"[Search settings]"
set hlsearch
set incsearch
set smartcase
set ignorecase
"set wrapscan
"[When on, the ":substitute" flag 'g' is default on]"
set nogdefault
"[Language settings]"
set iminsert=0
set imsearch=0
set helplang=en
set langmenu=en
language messages POSIX
"[Show DocName in title]"
set title
"[Hide/show the white-space and more invisible symbols]"
set list
set listchars=tab:\|\ ,trail:-
set nojoinspaces
"[Folding settings]"
set foldenable
set foldclose=""
set foldcolumn=1
set foldlevel=10
set foldlevelstart=10
set foldmethod=indent
set foldopen=block,hor,mark,percent,quickfix,search,tag,undo
"[Set to auto-read and to auto-write when
" a file is changed from the outside]"
set autoread
set autowrite
"[Vim will change the current working directory whenever you
" open a file, switch buffers, delete a buffer or open/close a window]"
set autochdir
"[Not be redrawn while executing macros,
" registers that have not been typed]"
set lazyredraw
"[Time-out settings]"
set timeout
set timeoutlen=3000
set ttimeoutlen=100
"[When off a buffer is unloaded when it is abandoned]"
set hidden
"[Switch between buffers. Settings]"
set switchbuf=useopen,usetab,newtab
"[Wrap settings]"
set nowrap
set whichwrap=<,>,[,]
set nolinebreak
"[Encoding/FileType settings]"
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8,cp1251,koi8-r
"[Use UNIX, DOS, MAC FileType]"
set fileformat=unix
set fileformats=unix,mac,dos
"[Better consistency between Windows and UNIX]"
set sessionoptions=""
set viewoptions=folds,options,cursor,unix,slash
set sessionoptions+=unix,slash,blank,buffers,curdir
set sessionoptions+=folds,help,options,tabpages,winsize
"[Completion settings]"
if has("autocmd")
    autocmd FileType c set omnifunc=ccomplete#Complete
    autocmd FileType css set omnifunc=csscomplete#CompleteCSS
    autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
    if has("ruby")
        autocmd FileType ruby set omnifunc=rubycomplete#Complete
    endif
    if has("perl")
        autocmd FileType perl set omnifunc=perlcomplete#CompletePERL
    endif
    if has("python")
        autocmd FileType python set omnifunc=pythoncomplete#Complete
    endif
    autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
endif
"[Completion search settings]"
set complete=.,b,d,i,k,s,t,u,U,w
set completeopt=longest,menuone,preview,menu
"[For jump between Java import statement]"
set includeexpr=substitute(v:fname,'\\.','/','g')
"[Determines the maximum number of items
" to show in the popup menu for Insert mode completion]"
set pumheight=7
"[No annoying sound or errors]"
set noerrorbells
set novisualbell
"[Shared with OS clipboard]"
set clipboard=unnamed
"[Abbreviations]"
cnoreabbrev W w
cnoreabbrev Q q
"[Turn on the wild menu and wild-mode settings]"
set wildmenu
set wildmode=list:longest,full
set wildignore=*.o,*.obj,*.pyc,*.pyo,*.swp,*.bak,*.exe,*.class
"[Use a GUI dialog when confirming]"
set confirm
"[Method used for encryption when the buffer is written to a file]"
if (version <= 702)
    set cryptmethod=zip
    else
    set cryptmethod=blowfish
endif
"[Make the scripts executable]"
function! ChangeScriptMode()
    if getline(1) =~ "#!"
        if getline(1) =~ "bin/"
            silent !chmod +x <afile>
        endif
    endif
endfunction
if has("unix") || has("mac")
    if has("autocmd")
        autocmd BufWritePost * call ChangeScriptMode()
    endif
endif
"[Python/Perl scripts templates]"
function! InitScriptFile(type)
    if (a:type == "python")
        execute setline(1, "#!/usr/bin/env python")
        execute setline(2, "# -*- coding: utf-8 -*-")
    elseif (a:type == "perl")
        execute setline(1, "#!/usr/bin/env perl")
        execute setline(2, "")
        execute setline(3, "use warnings;")
        execute setline(4, "use strict;")
    endif
    normal Go
endfunction
if has("autocmd")
    autocmd BufNewFile *.pl,*.pm call InitScriptFile("perl")
    autocmd BufNewFile *.py,*.pyw call InitScriptFile("python")
endif
"[Remove tabs and spaces at the end of lines]"
function! DeleteTrailingTWS()
    if &ft =~ 'diff'
        return
    end
    normal mb
    silent %s/[ \t]*$//g
    silent %s/\s\+$//ge
    normal 'b
endfunction
if has("autocmd")
    autocmd BufWritePre *.py,*.pyw retab
    autocmd BufWritePre * call DeleteTrailingTWS()
endif
"[Show current mode in StatusLine]"
function! ShowModeInStatusLine()
    let g:currentMode = mode()
    let g:showMode = ""
    if (g:currentMode ==# "i")
        let g:showMode = "Insert"
    elseif (g:currentMode ==# "R")
        let g:showMode = "Replace"
    elseif (g:currentMode ==# "n")
        let g:showMode = "Normal"
    else
        let g:showMode = "Visual"
    endif
    return g:showMode
endfunction
