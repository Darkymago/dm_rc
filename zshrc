# zshrc inspiré de celui de la formation debian de VIA
# modifié par David_5_1
# http://formation-debian.via.ecp.fr/
# Ajouts depuis le wiki de archlinux http://wiki.archlinux.fr/Zsh
# et depuis http://doc.ubuntu-fr.org/zsh
# et depuis http://zsh.sourceforge.net/Guide/zshguide.html
# utilisation de https://github.com/zsh-users/zsh-syntax-highlighting

# adapté et amélioré par lhark


export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin:~/bin
#:/opt/cuda/bin
export EDITOR=/usr/bin/vim
# Prevent mime associations by wine
#export WINEDLLOVERRIDES="winemenubuilder.exe=d"
#export CUDA_ROOT=/opt/cuda


# Get rc dir path
RC_PATH=$(dirname "$(readlink -f ${(%):-%x})")
echo $RC_PATH


# Import utility functions
. "$RC_PATH/zsh_scripts/functions"


# Check for rc updates in the background
(check_rc_update&) 2> /dev/null


# Check for system updates inthe background
(check_sys_update&) 2> /dev/null


# Configuration for virtualenv
export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh > /dev/null 2>&1
source /usr/bin/virtualenvwrapper.sh > /dev/null 2>&1


# Configuration for ROS
source /opt/ros/indigo/setup.zsh > /dev/null 2>&1
export PYTHONPATH=/opt/ros/indigo/lib/python2.7/site-packages:$PYTHONPATH
export PKG_CONFIG_PATH="/opt/ros/indigo/lib/pkgconfig:$PKG_CONFIG_PATH"


###########
# Aliases #
###########

alias acs='apt-cache search'
alias ls='ls --classify --tabsize=0 --literal --color=auto --show-control-chars -h'
alias ll='ls -lha'
alias less='less --quiet'
alias df='df --human-readable'
alias du='du --human-readable'
# alias mutt='mutt -y'
alias upgrade='sudo apt-get update && sudo apt-get -dy dist-upgrade && sudo apt-get dist-upgrade'
# alias -g GP='|grep '
alias grep="grep --color"
alias ssh='ssh -A'
# alias -s txt=cat
alias rm='rm -I'
alias ipa='ip route && echo && ip address'
alias ipr='ip address && echo && ip route'
alias which='which -a'
# Le fameux cd ...etc
alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'
alias -g ......='../../../../..'
alias -g .......='../../../../../..'
alias -g ........='../../../../../../..'
# Hackish tweaks
alias steam-wine='WINEDEBUG=-all wine ~/.wine/drive_c/Program\ Files\ \(x86\)/Steam/Steam.exe -no-dwrite -no-cef-sandbox >/dev/null 2>&1 &'
alias pacaur='AURDEST=$HOME/aur pacaur'
alias redwm='cd ~/aur/dwm-git; updpkgsums; makepkg -fi --noconfirm; killall dwm'
alias minecraft='java -jar $HOME/.minecraft/launcher.jar'
alias steam="LD_PRELOAD='/usr/\$LIB/libstdc++.so.6 /usr/\$LIB/libgcc_s.so.1 /usr/\$LIB/libxcb.so.1 /usr/\$LIB/libgpg-error.so' /usr/bin/steam"
alias surftor='http_proxy=socks5://127.0.0.1:9050/ surf'
# tor
alias torlaunch='sudo chroot --userspec=tor:tor /opt/torchroot /usr/bin/tor'


# Term specific hacks
case $TERM in
    xterm*)
      preexec () {
        print -Pn "\e]0;xterm - $1 - ${HOST} - ${PWD}\a"
      }
    ;;
    # Fix broken ssh with st
    st*)
      preexec () {
        print -Pn "\e]0;st - $1 - ${HOST} - ${PWD}\a"
      }
        alias ssh='TERM=xterm ssh -A'
    ;;
esac


###############
# ZSH options #
###############

unsetopt rm_star_silent     # Demande confirmation pour 'rm *' -> ou 'rm patth/*'
unsetopt glob_dots          # (disabled) Do not require a leading `.' in a filename to be matched explicitly.
setopt null_glob            # Delete pattern when no match found, instead of erroring
# (disabled) If there is an unambiguous prefix to insert on
# the command line, that is done without a completion list being displayed
#setopt list_ambiguous
# >| doit être utilisés pour pouvoir écraser un fichier déjà existant ;
# # le fichier ne sera pas écrasé avec '>'
#unsetopt clobber
setopt auto_remove_slash    # Auto remove slash at the end of autocomp when appropriate
setopt auto_cd              # Perform cd if command is directory and can't be executed
setopt chase_links          # Traite les liens symboliques comme il faut
setopt hist_verify          # !! n'est pas exécuté directement
setopt promptsubst          # Enable prompt substitution: vars are recalculated. you need '$foo'
setopt hist_ignore_all_dups # Only keep last version of duplicate command
setopt sharehistory         # Import new cmds from hist file & append typed cmds
setopt hist_ignore_space    # Ignore cmds with leading space
setopt print_exit_value     # Afficher «zsh: exit ERREUR» en cas d'erreur ≠ 0
setopt autopushd            # cd pushes old dir to dir stack
setopt pushdsilent          # Silent pushd & popd
setopt pushdtohome          # pushd = pushd $HOME
setopt pushdignoredups
setopt pushdminus           # This reverts the +/- operators for pushd.
setopt extendedglob         # Treat `#', `~' and `^' chars as part of patterns for filenames, etc.
setopt correct              # Provide correction to mistyped commands

EXPORTTIME=0                # (Disabled) Display command execution time
COMPLETION_WAITING_DOTS="true" # Display red dots while zsh autocompletes
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE=~/.history
HISTTIMEFORMAT="%d/%m %H:%M:%S "


######################
# Completion options #
######################

zstyle ':vcs_info:*' enable git cvs svn
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}'
zstyle ':completion:*' max-errors 3 numeric
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
# Des couleurs pour la complétion cf kill -9 <tab><tab>
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=36=31"
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
zstyle ':mime:*' mailcap /etc/mailcap
# video/*; /usr/bin/mplayer '%s';
# text/*; less '%s';
# audio/*; /usr/bin/mplayer '%s';
# Crée un cache des complétion possibles
# très utile pour les complétion qui demandent beaucoup de temps
# comme la recherche d'un paquet aptitude install moz<tab>
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zs


######################
# Load ZSH functions #
######################

zmodload zsh/complist
autoload -U compinit promptinit colors
compinit
promptinit
colors
autoload -Uz vcs_info
autoload -U zsh-mime-setup
autoload -U zsh-mime-handler
zsh-mime-setup


###################
# Prompt creation #
###################

prompt_create


##################
# Keyboard setup #
##################

bindkey -v # Vim type keybinds for ZLE (line editing)

# Gestion des touches spéciales pour mon clavier
# Pour connaître le code d'une touche, exécuter «cat»
typeset -A key
# CTRL + flèches
bindkey ";5D" beginning-of-line
bindkey ";5C" end-of-line
bindkey ";5A" up-line-or-history
bindkey ";5B" down-line-or-history
bindkey "^[[1;5D" beginning-of-line
bindkey "^[[1;5C" end-of-line
bindkey "^[[1;5A" history-incremental-search-backward
bindkey "^[[1;5B" history-incremental-search-forward
# Page Up/Down
bindkey "^[[5~" history-beginning-search-backward
bindkey "^[[6~" history-beginning-search-forward
# CTRL + Page UP/Down
bindkey "^[[5;5~" beginning-of-buffer-or-history
bindkey "^[[6;5~" end-of-buffer-or-history
# Origine / Fin (pavé numérique)
bindkey "^[[H" beginning-of-line
bindkey "^[[4~" end-of-line
## Origine / Fin
bindkey "^[OH" beginning-of-line
bindkey "^[OF" end-of-line
# Delete
bindkey "[3~" delete-char
# Shift-Tab
bindkey "^[[Z" reverse-menu-complete


# ZSH syntax highlighting : /!\ keep at the bottom !
source "${RC_PATH}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

# Add Miniconda to PATH
export PATH="/home/dmago/miniconda3/bin:$PATH"
